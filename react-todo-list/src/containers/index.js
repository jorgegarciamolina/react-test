import List from './List/List';
import Add from './Add/Add';
import Edit from './Edit/Edit';
import NotFound from './NotFound/NotFound';

export {
  List,
  Add,
  Edit,
  NotFound
}