import React from "react";
import "./Header.css";
import { Menu, Loader, Notification } from '../../components';
import { Get } from '../../services/api';
import propTypes from 'prop-types';

//Para el uso de Axios no es necesario la constante de FETCH_OPTIONS

class Header extends React.Component {
  
  render() {
    return (
      <div className="Header">
        <div className="Header__logo">
        <Get url="general" fetchAfterMount>
            {({ data, loading, error }) => {
                if (error) {
                    return (
                        <Notification type="error"
                            message= {error.message}
                        />
                    );
                }
                if (loading) {
                    return <Loader />;
                }
                if (data && data.logo) {
                    return (
                        <img alt="Accenture Logo"
                            src={data.logo}
                        />
                    );
                }
                return <Loader />;
            }}
        </Get>
    </div>
        <h1 className="Header__title">II OPENATHON Custom Open Cloud</h1>
        <Menu />
      </div>
    );
  }
}

Header.propTypes = {
  logo: propTypes.string
}

export default Header;
