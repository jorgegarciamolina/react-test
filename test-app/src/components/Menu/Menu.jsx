import React from "react";
import "./Menu.css";
import { Link } from "react-router-dom";

class Menu extends React.Component {
  render() {
    return (
      <div className="Menu">
        {this.state.expandedMenu && (
          <nav>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/services">Services</Link>
              </li>
              <li>
                <Link to="/innovation">Innovation</Link>
              </li>
              <li>
                <Link to="/guestbook">Guestbook</Link>
              </li>
            </ul>
          </nav>
        )}
        <button
          onClick={this.toggleMenu}
          className={`Menu__button ${
            this.state.expandedMenu ? "Menu__button--expanded" : ""
          }`}
        ></button>
      </div>
    );
  }

  constructor(props) {
    super(props);
    this.state = {
      expandedMenu: true
    };
    this.toggleMenu = this.toggleMenu.bind(this);
  }

  toggleMenu() {
    this.setState({ expandedMenu: !this.state.expandedMenu });
  }
}

export default Menu;
